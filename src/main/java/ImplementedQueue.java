import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ImplementedQueue<E> {
    private int max;
    private Queue<E> queue = new LinkedList<>();
    private ReentrantLock lock = new ReentrantLock(true);

    private Condition notEmpty = lock.newCondition();
    private Condition notFull = lock.newCondition();

    public ImplementedQueue(int size) {
        queue = new LinkedList<>();
        this.max = size;
    }
    public int getSize() {
        return queue.size();
    }
    public void offer(E e) {
        lock.lock();
        try{
            // block until the queue has place for new task
            if (queue.size() == max) {
                // wait until there is an empty place
                // (wait for notFull signal)
            notFull.wait();
        }
        queue.add(e);
            //send signal(notify) that there is task to take
        notEmpty.signalAll();
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally{
        lock.unlock();
        }
    }
    public E take () {
        lock.lock ( );
        try{
            // lock until the queue has at least one task
            while ( queue.size ( ) == 0 ) {
                // wait until notEmpty signal is received
            notEmpty.wait();}
            E item = queue. remove();
            //send signal (notify) that the queue has an empty place
            notFull.signalAll();
            return item;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally{
            lock. unlock ( ) ;
        }
    }
}
