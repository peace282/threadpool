public class ThreadPoolMain {

    public static void main(String[] args) throws Exception {

        int maxNoOfTasks = 1000;
        ThreadPool threadPool = new ThreadPool(3, maxNoOfTasks);
        long start = System.currentTimeMillis();
        long timeout = 50;

        for(int i=0; i<maxNoOfTasks; i++) {
            int taskNo = i;
            threadPool.execute( () -> {
                String message =
                        Thread.currentThread().getName()
                                + ": Task " + taskNo ;
                System.out.println(message);
            });
        }
        threadPool.waitUntilAllTasksFinished(timeout);
        threadPool.stop();
        long end = System.currentTimeMillis();
//        System.out.println("Required Time for "+maxNoOfTasks+" task is:" + (end - start));

    }
}
